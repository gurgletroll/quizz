
//Tableaux de questions

export let questionsT1 = [
    {
        question: "Qui est le maître Jedi d'Obi-Wan Kenobi dans l'Épisode I ?",
        choix1: "Yoda",
        choix2: "Ki Adi Mundi",
        choix3: "Kit Fisto",
        answer: "Qui-Gon Jinn"
    },
    {
        question: "Quel est le titre de l'Épisode I de Star Wars ?",
        choix1: "L'Attaque des Clones",
        choix2: "La Revanche des Sith",
        choix3: "Le Retour du Jedi",
        answer: "La Menace Fantôme"
    },
    {
        question: "Comment s'appelle le général cyborg des Séparatistes dans l'Épisode III ?",
        choix1: "Comte Dooku",
        choix2: "Nute Gunray",
        choix3: "Sebulba",
        answer: "Général Grievous"
    },
    {
        question: "Qui est la reine de Naboo dans l'Épisode I ?",
        choix1: "Lana Del Rey",
        choix2: "Mara Jade",
        choix3: "Cara Duune",
        answer: "Padmé Amidala"
    },
    {
        question: "Sur quelle planète Anakin massacre-t-il les Tusken Raiders dans l'Épisode II ?",
        choix1: "Dantooine",
        choix2: "Bespin",
        choix3: "Dromund Kaas",
        answer: "Tatooine"
    },
    {
        question: "Quel est le nom de la planète où Anakin et Padmé se marient en secret ?",
        choix1: "Kamino",
        choix2: "Korriban",
        choix3: "Endor",
        answer: "Naboo"
    },
    {
        question: "Qui est le père de Boba Fett ?",
        choix1: "Rolo Fett",
        choix2: "Anton Fett",
        choix3: "Jano Fett",
        answer: "Jango Fett"
    },
    {
        question: "Qui tue Jango Fett dans l'Épisode II ?",
        choix1: "Pre Vizsla",
        choix2: "Jar Jar Binks",
        choix3: "Kit Fisto",
        answer: "Mace Windu"
    },
    {
        question: "Quel est le nom de la planète océanique où se trouve le complexe de clonage dans l'Épisode II ?",
        choix1: "Coruscant",
        choix2: "Genabackis",
        choix3: "Alderaan",
        answer: "Kamino"
    },
    {
        question: "Quel est le nom donné à l'ordre d'extermination des Jedi dans l'Épisode III ?",
        choix1: "L'Ordre suprême",
        choix2: "L'Ordre final",
        choix3: "L'Ordre 87",
        answer: "L'Ordre 66"
    }
];


export let questionsT2 = [
    {
        question: "Qui est le père de Luke Skywalker ?",
        choix1: "Obi-Wan Skywalker",
        choix2: "Han Skywalker",
        choix3: "Lando Skywalker",
        answer: "Anakin Skywalker"
    },
    {
        question: "Comment s'appelle la princesse que Luke et Han Solo sauvent dans l'Épisode IV ?",
        choix1: "Mon Mothma",
        choix2: "Padmé Amidala",
        choix3: "Rey",
        answer: "Leia Organa"
    },
    {
        question: "Quel est le nom du célèbre chasseur de primes qui capture Han Solo dans l'Épisode V ?",
        choix1: "Jango Fett",
        choix2: "Bossk",
        choix3: "IG-88",
        answer: "Boba Fett"
    },
    {
        question: "Sur quelle planète se trouve la base de l'Empire dans l'Épisode V ?",
        choix1: "Naboo",
        choix2: "Mustafar",
        choix3: "Endor",
        answer: "Hoth"
    },
    {
        question: "Qui est le leader de l'Empire Galactique ?",
        choix1: "Grand Moff Tarkin",
        choix2: "Comte Dooku",
        choix3: "General Grievous",
        answer: "Empereur Palpatine"
    },
    {
        question: "Quel est le nom du petit astromech droid de Luke ?",
        choix1: "C-3PO",
        choix2: "BB-8",
        choix3: "K-2SO",
        answer: "R2-D2"
    },
    {
        question: "Qui dirige l'attaque contre l'Étoile de la Mort dans l'Épisode VI ?",
        choix1: "Marechal Galbot",
        choix2: "Mon Mothma",
        choix3: "General Rieekan",
        answer: "Admiral Ackbar"
    },
    {
        question: "Quel est le nom de la lune forestière où se trouve le générateur de bouclier de l'Etoile de la mort ?",
        choix1: "Dagobah",
        choix2: "Tatooine",
        choix3: "Jakku",
        answer: "Endor"
    },
    {
        question: "Qui tue l'Empereur Palpatine dans l'Épisode VI ?",
        choix1: "Luke Skywalker",
        choix2: "Leia Organa",
        choix3: "Han Solo",
        answer: "Darth Vader"
    },
    {
        question: "Quel personnage prononce la réplique : 'Je suis ton père' ?",
        choix1: "Obi-Wan Kenobi",
        choix2: "Yoda",
        choix3: "Han Solo",
        answer: "Darth Vader"
    }
];