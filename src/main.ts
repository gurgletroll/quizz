import { questionsT1,questionsT2 } from "./entities"

const q1 = document.querySelector<HTMLElement>('.Q1');
const q2 = document.querySelector<HTMLElement>('.Q2');
const q3 = document.querySelector<HTMLElement>('.Q3');
const q4 = document.querySelector<HTMLElement>('.Q4');

const bouton1 = document.querySelector<HTMLButtonElement>('#btnPush1');
const bouton2 = document.querySelector<HTMLButtonElement>('#btnPush2');
const bouton3 = document.querySelector<HTMLButtonElement>('#btnPush3');
const bouton4 = document.querySelector<HTMLButtonElement>('#btnPush4');


const currentQ = document.querySelector<HTMLElement>('#question');
const btn = document.querySelector<HTMLButtonElement>('#next');

const carte1 = document.querySelector<HTMLElement>('.cardcolor1');
const carte2 = document.querySelector<HTMLElement>('.cardcolor2');
const carte3 = document.querySelector<HTMLElement>('.cardcolor3');
const carte4 = document.querySelector<HTMLElement>('.cardcolor4');

const selector = document.querySelector<HTMLSelectElement>('#selectQuizz')

const fin = document.querySelector<HTMLElement>('#messageFin');

const currentScore = document.querySelector<HTMLElement>('#currentScore')

const container = document.querySelector<HTMLElement>('.container');
const numQuestion = document.querySelector<HTMLElement>('.questionEnCours');

const msgFin = document.querySelector<HTMLElement>('#paraFin');


let bolTest = false;
let bolTest2 = false;
let bolTest3 = false;
let bolTest4 = false;

let audio = new Audio('/sounds/faith.mp3');
let audio2 = new Audio('/sounds/R2.mp3')

let compteur = 10;
let score = 0;
let questionEnCours = 1;


function getRandomIndex(array: string | any[]) {

    const randomIndex = Math.floor(Math.random() * array.length);

    return randomIndex;
}

function melangerArray(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]]; // Algo de Fisher-Yates
    }
}

function highlight() {

    let bol = [bolTest, bolTest2, bolTest3, bolTest4];
    let carteBonneCouleur = [carte1, carte2, carte3, carte4];

    for (let i = 0; i < 4; i++) {
        if (bol[i] == true) {
            carteBonneCouleur[i].classList.add('cardGood');
            console.log('fonction', bol);
        }else{carteBonneCouleur[i].classList.add('cardBad');}
    }
}

function disableAll() {

    btn.disabled = true;
    bouton1.disabled = true;
    bouton2.disabled = true;
    bouton3.disabled = true;
    bouton3.disabled = true;
    selector.disabled=true;

}

function disableAnswers(){
    bouton1.disabled = true;
    bouton2.disabled = true;
    bouton3.disabled = true;
    bouton4.disabled = true;

}

function finPartie(){

    if (compteur==0){
        disableAll();
        
        

        setTimeout(function() {
            fin.style.display = 'flex';
            container.style.opacity = '0.4';

            if (score>8){
                msgFin.innerHTML= 'Votre score : ' + score + '<br>' +'  Que la force soit avec vous';
            }else if (score>5){
                msgFin.innerHTML= 'Votre score : ' + score + '<br>' +'  Bien joué Chef';
            }else {
                msgFin.innerHTML= 'Votre score : ' + score + '<br>' +'  Direction le Sarlaac';
            }
            
        }, 2000)

    }
}

btn.addEventListener('click', () => {

    
    
    
    numQuestion.style.display = 'block';
    container.style.display = 'block';
    btn.textContent = 'Question suivante';
    numQuestion.textContent='Question n°' + String(questionEnCours);


    const choix = selector.value;
    
    
    let tableauQuestions;
    if (choix == 'option1') {
        tableauQuestions = questionsT1;
    } else if (choix == 'option2') {
        tableauQuestions = questionsT2;
    }

    carte1.classList.remove('cardGood');
    carte2.classList.remove('cardGood');
    carte3.classList.remove('cardGood');
    carte4.classList.remove('cardGood');

    carte1.classList.remove('cardBad');
    carte2.classList.remove('cardBad');
    carte3.classList.remove('cardBad');
    carte4.classList.remove('cardBad');

    bouton1.disabled = false;
    bouton2.disabled = false;
    bouton3.disabled = false;
    bouton4.disabled = false;

    bolTest = false;
    bolTest2 = false;
    bolTest3 = false;
    bolTest4 = false;

    let a = getRandomIndex(tableauQuestions);
    let question = tableauQuestions[a].question;
    currentQ.textContent = question;
    let elements = [tableauQuestions[a].choix1, tableauQuestions[a].choix2, tableauQuestions[a].choix3, tableauQuestions[a].answer];
    melangerArray(elements);
    let [b, c, d, e] = elements;
    q1.textContent = b;
    q2.textContent = c;
    q3.textContent = d;
    q4.textContent = e;

    if (b == tableauQuestions[a].answer) {
        console.log(b, '=', tableauQuestions[a].answer);
        bolTest = true;


    } else if (c == tableauQuestions[a].answer) {
        console.log(c, '=', tableauQuestions[a].answer);
        bolTest2 = true;


    } else if (d == tableauQuestions[a].answer) {
        console.log(d, '=', tableauQuestions[a].answer);
        bolTest3 = true;


    } else if (e == tableauQuestions[a].answer) {
        console.log(e, '=', tableauQuestions[a].answer);
        bolTest4 = true;


    }

    console.log('test des bol', bolTest, bolTest2, bolTest3, bolTest4);

    tableauQuestions.splice(a, 1);

    compteur = compteur-1;
    questionEnCours = questionEnCours+1;
    console.log('compteur : ',compteur);
    console.log('score: ',score);
    

    selector.disabled=true;

    if (compteur==0){
        btn.disabled=true
    }

}
)

bouton1.addEventListener('click', () => {

    if (bolTest) {
        score = score+1;
        audio2.play();
    } else {

        audio.play();
        
        


    }
    currentScore.textContent='Score : ' + String(score);
    highlight();
    disableAnswers();


    bolTest = false;

    finPartie();
    

    
})

bouton2.addEventListener('click', () => {

    if (bolTest2) {
        score = score+1;
        audio2.play();
    } else {
        audio.play();
        
    }
    currentScore.textContent='Score : ' + String(score);
    highlight()
    bolTest2 = false;
    disableAnswers();
    finPartie();


})

bouton3.addEventListener('click', () => {

    if (bolTest3) {
        audio2.play();
        score = score+1;
    } else {
        audio.play();
        
    }
    currentScore.textContent='Score : ' + String(score);
    highlight()
    bolTest4 = false;

    disableAnswers();
    finPartie();


})

bouton4.addEventListener('click', () => {

    if (bolTest4) {
        score = score+1;
        audio2.play();
    } else {
        audio.play();
        
    }
    currentScore.textContent='Score : ' + String(score);
    highlight()
    bolTest4 = false;

    disableAnswers();
    finPartie();


})











