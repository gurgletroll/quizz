Pour ce projet, l'une des fonctionnalités que j'ai essayées de résoudre en premier était le fait d'avoir un set de questions affichées aléatoirement, et un ensemble de données brutes facilement manipulables. Je me suis servi d'un tableau d'objets, ce qui me permettait d'avoir un quizz facilement modifiable et virtuellement infini. Pour l'aléatoire, après quelques recherches, je suis tombé sur le mélande de fisher-yates, un algorithme relativement simple permettant de générer de l'aléatoire.


La plus grande difficulté a résidée dans le fait de trouver comment trigger certains éléments, et comment détécter si une réponse est juste ou non, en gros comment trouver un algo général qui détéctait une bonne réponse d'une mauvaise et agissait en fonction. Pour cela, je suis passé par un ensemble de booléens. J'ai également fait en sorte de minimiser les répétitions avec la déclaration de plusieurs fonctions, mais il est probable que certaines redondances demeurent dans le code.

lien vers le Quizz déployé : https://master--comfy-tanuki-1af009.netlify.app/

Maquette fonctionnelle du projet : ![maquetteFonctionnelle](maquette.png)

